import java.util.Random;

public class Board {
    private Tile[][] grid;
    private final int SIZE = 5;
    
    public Board() {
        this.grid = new Tile[this.SIZE][this.SIZE];
        for (int i = 0; i < this.SIZE; i++) {
            for (int j = 0; j < this.SIZE; j++) {
                this.grid[i][j] = Tile.BLANK;
            }
        }

        Random rng = new Random();
        for (int i = 0; i < this.SIZE; i++) {
            this.grid[i][rng.nextInt(this.grid[i].length)] = Tile.HIDDEN_WALL;
        }
    }
    
    public String toString() {
        String gridString = "\n ";
        for (Tile[] row : this.grid) {
            int i = 0;
            for (Tile tile : row) {
                gridString += tile.getName();
                if (i < row.length - 1) {
                    gridString += " | ";
                }
                i++;
            }
            gridString += "\n";
            if (row != grid[this.SIZE - 1]) {
                for (int j = 0; j < this.SIZE - 1; j++) {
                    gridString += "---+";
                }
                gridString += "---\n ";
            }
        }
        return gridString;
    }

    public int placeToken(int row, int column) {
        if (row >= 0 && column >= 0) {
            if (row >= this.SIZE || column >= this.SIZE) {
                return -2;
            } else if (this.grid[row][column] == Tile.CASTLE || this.grid[row][column] == Tile.WALL) {
                return -1;
            } else if (this.grid[row][column] == Tile.HIDDEN_WALL) {
                this.grid[row][column] = Tile.WALL;
                return 1;
            } else {
                this.grid[row][column] = Tile.CASTLE;
                return 0;
            }
        } else {
            return -2;
        }
    }
}
