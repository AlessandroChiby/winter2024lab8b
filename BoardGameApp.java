import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("\033[H\033[2J");

        Board board = new Board();
        int numCastles = 6;
        int turns = 8;
        System.out.println("Welcome!");

        int row = 0;
        int column = 0;

        while (numCastles > 0 && turns > 0) {
            System.out.println(board);
            System.out.println("Number of castles to place: " + numCastles);
            System.out.println("Turns: " + turns + "\n");

            System.out.print("Enter Row (0-4): ");
            row = reader.nextInt();
            System.out.print("Enter Column (0-4): ");
            column = reader.nextInt();
            System.out.print("\033[H\033[2J");


            int result = board.placeToken(row, column);

            if (result == 1) {
                System.out.println("There was a wall!");
                turns--;
            } else if (result == 0) {
                System.out.println("Castle successfully placed!");
                turns--;
                numCastles--;
            } else {
                System.out.println(" ");
            }
        }
        System.out.println(board);
        if (numCastles == 0) {
            System.out.println("You won!");
        } else {
            System.out.println("Better luck next time!");
        }
        System.out.println(" ");
    }
}