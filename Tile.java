enum Tile {
    BLANK(" "),
    HIDDEN_WALL(" "),
    WALL("W"),
    CASTLE("C");

    private String name;
    private Tile(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
}